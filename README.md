# Colored Coins OP_RETURN decoder

**techincolor** is a [Colored
Coins](https://github.com/Colored-Coins/Colored-Coins-Protocol-Specification)
transaction metadata decoder.

## Environment setup

The shell script `run.sh` builds a
[docker](https://docs.docker.com/install/ "docker install docs") image and runs
it. It provides all the required software to run the scripts.

    $ ./run.sh

Otherwise, you could use NodeJs 8 and install dependencies into the project
directory.

    $ npm install

## OP_RETURN data

Ideally you should retrieve `OP_RETURN` data of a Colored Coins transaction
with your Bitcoin fullnode (for reliability and trust).

As an example we will look at the TXID
`0784288108ed64aa7edf3407922096fd19754881b5647fa72be8c06c0f490111`, and look
back until we find the issuance transaction (this is one of the initial
cascading distribution transactions, so expect a LOT of sanuscoins).


Get the `OP_RETURN` data via `bitcoin-cli`:

    $ T=$(bitcoin-cli getrawtransaction 0784288108ed64aa7edf3407922096fd19754881b5647fa72be8c06c0f490111)
    $ bitcoin-cli decoderawtransaction $T | jq '.vout[1].scriptPubKey.hex'
    "6a144343021500c014b811340cc701c0070ddcd0a140"

Alternatively, you can use third party block explorers like [Blockstream's
Esplora](https://blockstream.info "Esplora") and
[Smartbit](https://www.smartbit.com.au/) to retrieve the `OP_RETURN`.
The provided tool `getdata.js` provides a convenient way to do this.
`getdata.js` fetches the `OP_RETURN` from both mentioned explorers and checks
the consistency of the reeceived data between the two services.

    $ node getdata.js 0784288108ed64aa7edf3407922096fd19754881b5647fa72be8c06c0f490111
    4343021500c014b811340cc701c0070ddcd0a140

## Run

The data accepted by decoder starts with the bytes `4343`.

The decoder does not accept the first two<sup>1</sup> bytes (opcode `6a` and
length of the Colored Coins script).

    $ node decoder.js <hex data>

Example:

    $ node decoder.js 4343021500c014b811340cc701c0070ddcd0a140

#### Notes

1. if the script is longer than 75 bytes, 2 bytes are needed to encode the
length

## Output of decoder.js

Example:

    {
        "protocol": 17219,
        "version": 2,
        "multiSig": [],
        "payments": [
            {
                "skip": false,
                "range": false,
                "percent": false,
                "output": 0,
                "amount": 22780795161799
            },
            {
                "skip": false,
                "range": false,
                "percent": false,
                "output": 1,
                "amount": 7756120629568
            }
        ]
    }


The list `payments` defines how input assets are assigned to transaction
outputs.  If `percent` is `true`, the `amount` is a percentage of the input
amount, the value is absolute otherwise.

Amounts are expressed in units of sanuscoin, often called _ewalds_. In this
example, 22780795161799 _ewalds_ (2278079.5161799 sanuscoins, ~2 million SACs)
are assigned to the first address with the UTXO
`0784288108ed64aa7edf3407922096fd19754881b5647fa72be8c06c0f490111:0` and
7756120629568 _ewalds_ are assigned to the second one. The field `output`
identifies the vout of the transaction.

All assets not explicitly assigned by the `payments` list to specific outputs
are implicitly assigned to the last output of the transaction, acting as a
change output.

## Recursion

In order to check the validity of the transaction and compute the asset change
amount, it is necessary to obtain the number of assets spent by the
transaction.  We need to compare the sum of assets spent by the current
transaction with the assets received from the parent transaction (using
`decoder.js` on the parent transaction).

In our example, the parent TXID is easy to spot as it has just 1 input, so
the parent TXID must be
`1027cc26862e539fcf99e772861742d66ff7acc5b25e73086319d4edbdfb2067`.

The parent of
`1027cc26862e539fcf99e772861742d66ff7acc5b25e73086319d4edbdfb2067` is a bit
difficult to examine via Bitcoin Core or explorer services, because we must
check each of the 3 inputs in order to see which one is providing the
sanuscoins. Two inputs refer to the same TXID
`ea7e64a02cce3dca5ea57e6356a42c01422aaaacb0ab222848d4d9d4ac3d5ab9` which is the
only one with `OP_RETURN` data and happens to be the issuance transaction.

As above the data we'll find in the `OP_RETURN` decodes as:

    $ node decoder.js 4343021500c1afa37803586601c0b73cba017def02c09b9d59a6d32c03c01bc5ee04ae0
    {
        "protocol": 17219,
        "version": 2,
        "multiSig": [],
        "payments": [
            {
            "skip": false,
            "range": false,
            "percent": false,
            "output": 0,
            "amount": 474591604725862
            },
            {
            "skip": false,
            "range": false,
            "percent": false,
            "output": 1,
            "amount": 201471446580719
            },
            {
            "skip": false,
            "range": false,
            "percent": false,
            "output": 2,
            "amount": 171100116276012
            },
            {
            "skip": false,
            "range": false,
            "percent": false,
            "output": 3,
            "amount": 30536915791367
            }
        ]
    }


Sanuscoins available for spending are assigned in output #3, so 30536915791367
_ewalds_. Subtracting the 2 amounts 22780795161799 and 7756120629568 from the
30536915791367 available leaves 0 remaining, meaning there were no _ewalds_
assigned to the last output of the former transaction.

### Follow the transactions flow

In order to retrieve the inputs of a transaction you could use `bitcoin-cli`
and `jq` or the provided tool `getinputs.js` to download data from block
explorers.

This is an example of a `bitcoin-cli` call:

    $ T=$(bitcoin-cli getrawtransaction 1027cc26862e539fcf99e772861742d66ff7acc5b25e73086319d4edbdfb2067)
    $ bitcoin-cli decoderawtransaction $T | jq '.vin | .[] | .txid, .vout'

This is an example of a `getinputs.js` call:

    $ node getinputs.js 1027cc26862e539fcf99e772861742d66ff7acc5b25e73086319d4edbdfb2067

## Issuance transactions

Issuance transactions are a specific type of transaction of Colored Coins
protocol. They create assets.

In order to verify the validity of an asset, the issuance transaction must be
provided by the issuer.

The `decoder.js` tool can interpret the `OP_RETURN` of an issuance transaction,
to do so append the `--issuance` option to the call.

In our example the TXID of the SanusCoin issuance is:

    ea7e64a02cce3dca5ea57e6356a42c01422aaaacb0ab222848d4d9d4ac3d5ab9

and the `OP_RETURN` data is

    434302058172df938f008034fb5e3f018034fb5e3f028034fb5e3f038034fb5e3f048034fb5e3f058034fb5e3f068034fb5e3ff0

Data decode:

    $ node decoder.js 434302058172df938f008034fb5e3f018034fb5e3f028034fb5e3f038034fb5e3f048034fb5e3f058034fb5e3f068034fb5e3ff0 --issuance
    {
    "divisibility": 7,
    "lockStatus": true,
    "aggregationPolicy": "aggregatable",
    "protocol": 17219,
    "version": 2,
    "multiSig": [],
    "noRules": true,
    "amount": 7777777770000000,
    "payments": [
        {
        "skip": false,
        "range": false,
        "percent": false,
        "output": 0,
        "amount": 1111111110000000
        },
        {
        "skip": false,
        "range": false,
        "percent": false,
        "output": 1,
        "amount": 1111111110000000
        },
        {
        "skip": false,
        "range": false,
        "percent": false,
        "output": 2,
        "amount": 1111111110000000
        },
        {
        "skip": false,
        "range": false,
        "percent": false,
        "output": 3,
        "amount": 1111111110000000
        },
        {
        "skip": false,
        "range": false,
        "percent": false,
        "output": 4,
        "amount": 1111111110000000
        },
        {
        "skip": false,
        "range": false,
        "percent": false,
        "output": 5,
        "amount": 1111111110000000
        },
        {
        "skip": false,
        "range": false,
        "percent": false,
        "output": 6,
        "amount": 1111111110000000
        }
    ]
    }

The decoded Colored Coins script shows that output #4 has 1111111110000000 _ewalds_, which satisfies the requirements of the aforementioned transaction.

All is verified, all is legit.
