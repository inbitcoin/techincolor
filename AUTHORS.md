Techincolor contributors, in chronological order of contribution:

* [Martino Salvetti](https://gitlab.com/inaltoasinistra) - tldr@inbitcoin.it
* [Marco Amadori](https://gitlab.com/mammadori) - amadori@inbitcoin.it
* [Nicola Busanello](https://gitlab.com/dieeasy) - sudo@inbitcoin.it
