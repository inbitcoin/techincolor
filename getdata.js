#!/usr/bin/env node

const help = `
USAGE:
  $ node getdata.js <txid>

Example:
  txid: 83612466c564525716d07e492f4207f622fde7c875982f017994143bba36f3e2
  op return: 43430215002017
  $ node getdata.js 83612466c564525716d07e492f4207f622fde7c875982f017994143bba36f3e2
`

const lib = require("./lib")

// Blockstream.info
// .vout[i] =>
// {
//   "scriptpubkey": "6a34434302058172df938f008034fb5e3f018034fb5e3f028034fb5e3f038034fb5e3f048034fb5e3f058034fb5e3f068034fb5e3ff0",
//   "scriptpubkey_asm": "OP_RETURN OP_PUSHBYTES_52 434302058172df938f008034fb5e3f018034fb5e3f028034fb5e3f038034fb5e3f048034fb5e3f058034fb5e3f068034fb5e3ff0",
//   "scriptpubkey_address": null,
//   "scriptpubkey_type": "op_return",
//   "value": 0
// },

async function getDataBlockstream(txid) {
  const url = lib.B_GET_TX + txid

  const tx = JSON.parse(await lib.getContent(url))
  const output = tx.vout.filter(output => output.scriptpubkey_type == 'op_return')[0]

  if (!output)
    throw new Error('OP_RETURN data not found')

  const hex = output.scriptpubkey_asm.split(' ')[2]
  return hex
}

// Smartbit
// .outputs[i] =>
// {
//   "addresses": [],
//   "value": "0.00000000",
//   "value_int": 0,
//   "n": 7,
//   "script_pub_key": {
//     "asm": "OP_RETURN 434302058172df938f008034fb5e3f018034fb5e3f028034fb5e3f038034fb5e3f048034fb5e3f058034fb5e3f068034fb5e3ff0",
//     "hex": "6a34434302058172df938f008034fb5e3f018034fb5e3f028034fb5e3f038034fb5e3f048034fb5e3f058034fb5e3f068034fb5e3ff0"
//   },
//   "req_sigs": 0,
//   "type": "nulldata",
//   "spend_txid": null
// },

async function getDataSmartBit(txid) {
  const url = lib.S_GET_TX + txid

  const tx = JSON.parse(await lib.getContent(url))
  if (!tx.success)
    throw new Error(`Blockchain explorer error\nurl: ${url}`)
  const output = tx.transaction.outputs.filter(output => output.type == 'nulldata')[0]

  if (!output)
    throw new Error('OP_RETURN data not found')

  const hex = output.script_pub_key.asm.split(' ')[1]
  return hex
}

async function getDataAndCompare(txid) {
  const bs = await getDataBlockstream(txid)
  const sb = await getDataSmartBit(txid)
  if (bs == sb)
    return bs
  throw new Error(`Blockstream and Smartbit OP_RETURN data are different\nBlockstream: ${bs}\nSmartbit: ${sb}`)
}

if (process.argv.length !== 3) {
  console.log(help)
} else {
  const txid = process.argv[2]
  getDataAndCompare(txid).then(hex => {
    console.log(hex)
  }).catch(err => {
    console.log(err.message)
  })
}
