FROM node:8.16.0-stretch-slim
ENV LANG en_US.UTF-8

ARG APP=/srv/app

RUN mkdir ${APP}
ADD . ${APP}

WORKDIR ${APP}
RUN npm install

CMD ["bash"]
