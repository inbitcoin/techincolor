#!/usr/bin/env node

const tEnc = require('cc-transfer-encoder')
const iEnc = require('cc-issuance-encoder')
const lib = require("./lib")

const help = `
USAGE:
  $ node decoder.js <hex encoded op_return of cc transaction> [--issuance]
OR
  $ node decoder.js - [--issuance]
to read hex encoded data from stdin

Example with OP_RETURN data as input:
  txid: 83612466c564525716d07e492f4207f622fde7c875982f017994143bba36f3e2
  op return: 6a0743430215002017
  $ node decoder.js 43430215002017

  NOTE: skip the OP_RETURN opcode (6a) and the length of data pushed
        on the stack (in the example: 07)

Example with txid as input:
  $ node getdata.js 83612466c564525716d07e492f4207f622fde7c875982f017994143bba36f3e2 | node decoder.js -

Example with the issuance transaction:
  $ node getdata.js ea7e64a02cce3dca5ea57e6356a42c01422aaaacb0ab222848d4d9d4ac3d5ab9 | node decoder.js - --issuance
`

var toBuffer = function (val) {
  val = val.toString(16)
  if (val.length % 2 == 1) {
    val = '0'+val
  }
  return new Buffer(val, 'hex')
}

function decode(hex, encoder) {
  try {
    const obj = encoder.decode(toBuffer(hex))
    console.log(JSON.stringify(obj, null, 2))
  } catch (e) {
    console.error(e.message)
  }
}

if (process.argv.length < 3) {
  console.log(help)
} else {
  const hex = process.argv[2]
  const isIssuance = process.argv[3] == '--issuance'
  const encoder = isIssuance ? iEnc : tEnc
  if (hex === '-') {
    lib.readStdin(function(hex) {
      decode(hex.trim(), encoder)
    })
  } else {
    decode(hex, encoder)
  }
}
