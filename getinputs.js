#!/usr/bin/env node

const help = `
USAGE:
  $ node getinputs.js <txid>

Example:
  txid: 83612466c564525716d07e492f4207f622fde7c875982f017994143bba36f3e2
  $ node getinputs.js 83612466c564525716d07e492f4207f622fde7c875982f017994143bba36f3e2
`

const _ = require("lodash")
const lib = require("./lib")

async function getInputsBlockstream(txid) {
  const url = lib.B_GET_TX + txid

  const tx = JSON.parse(await lib.getContent(url))
  const inputs = tx.vin.map(input => `${input.txid}:${input.vout}`)

  return inputs
}

async function getInputsSmartBit(txid) {
  const url = lib.S_GET_TX + txid

  const tx = JSON.parse(await lib.getContent(url))
  if (!tx.success)
    throw new Error(`Blockchain explorer error\nurl: ${url}`)
  const inputs = tx.transaction.inputs.map(input => `${input.txid}:${input.vout}`)

  return inputs
}

async function getInputsAndCompare(txid) {
  const bs = await getInputsBlockstream(txid)
  const sb = await getInputsSmartBit(txid)
  if (_.isEqual(bs, sb))
    return bs
  throw new Error(`Blockstream and Smartbit OP_RETURN data are different\nBlockstream: ${bs}\nSmartbit: ${sb}`)
}

if (process.argv.length !== 3) {
  console.log(help)
} else {
  const txid = process.argv[2]
  getInputsAndCompare(txid).then(inputs => {
    console.log(JSON.stringify(inputs, null, 2))
  }).catch(err => {
    console.log(err.message)
  })
}
