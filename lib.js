const zlib = require("zlib")

exports.B_GET_TX = 'https://blockstream.info/api/tx/'
exports.S_GET_TX = 'https://api.smartbit.com.au/v1/blockchain/tx/'

exports.readStdin = function(cb) {
  const stdin = process.stdin
  var inputChunks = []
  
  stdin.resume()
  stdin.setEncoding('utf8')

  stdin.on('data', function (chunk) {
      inputChunks.push(chunk);
  })

  stdin.on('end', function () {
      return cb(inputChunks.join(''))
  })
}


exports.getContent = function(url) {
  // return new pending promise
  return new Promise((resolve, reject) => {
    // select http or https module, depending on reqested url
    const lib = url.startsWith('https') ? require('https') : require('http')
    const request = lib.get(url, (response) => {
      // handle http errors
      if (response.statusCode < 200 || response.statusCode > 299) {
         reject(new Error(`Failed to load page, status code: ${response.statusCode}\nurl: ${url}`))
      }
      // temporary data holder
      const body = []
      // on every content chunk, push it to the data array
      response.on('data', (chunk) => body.push(chunk))
      // we are done, resolve promise with those joined chunks
      response.on('end', () => {
        if (response.headers['content-encoding'] == 'gzip') {
          const content = Buffer.concat(body)
          return zlib.gunzip(content, function(err, dezipped) {
            if (err) {
              console.error(url)
              throw err
            }
            return resolve(dezipped.toString())
          })
        } else {
          const content = body.join('')
          return resolve(content)
        }
      })
    })
    // handle connection errors of the request
    request.on('error', (err) => reject(err))
    request.end()
  })
}
